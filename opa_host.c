/*
 * opa_host.c
 *
 * OPA host record.
 */

#include "opa_host.h"

typedef struct opa_host {
    unsigned int    refcnt;
    char            *name;
    STL_LID         opa_lid;
    uint64_t        opa_guid;
} opa_host_t;

//

opa_host_ref
opa_host_alloc(
    const char      *name,
    STL_LID         opa_lid,
    uint64_t        opa_guid
)
{
    size_t          name_len = 0;
    
    // Isolate the name to the first whitespace:
    while ( name[name_len] && ! isspace(name[name_len]) ) name_len++;
    
    opa_host_t      *new_host = (opa_host_ref)malloc(sizeof(opa_host_t) + (name_len + 1));
    
    if ( new_host ) {
        new_host->refcnt = 1;
        new_host->opa_lid = opa_lid;
        new_host->opa_guid = opa_guid;
        new_host->name = ((void*)new_host) + sizeof(opa_host_t);
        strncpy(new_host->name, name, name_len);
        new_host->name[name_len] = '\0';
    }
    return new_host;
}

//

opa_host_ref
opa_host_retain(
    opa_host_ref    host
)
{
    host->refcnt++;
    return host;
}

//

void
opa_host_release(
    opa_host_ref    host
)
{
    if ( --host->refcnt == 0 ) {
        free((void*)host);
    }
}

//

const char*
opa_host_name(
    opa_host_ref    host
)
{
    return host->name;
}

//

STL_LID
opa_host_lid(
    opa_host_ref    host
)
{
    return host->opa_lid;
}

//

uint64_t
opa_host_guid(
    opa_host_ref    host
)
{
    return host->opa_guid;
}

//

struct opa_host_lid_search_context {
    STL_LID         target_lid;
    unsigned int    insert_at;
    bool            search_okay;
};

bool
__opa_host_lid_search(
    unsigned int    i,
    const void      *host,
    const void      *context
)
{
    opa_host_t      *HOST = (opa_host_ref)host;
    struct opa_host_lid_search_context *CONTEXT = (struct opa_host_lid_search_context*)context;
    
    if ( CONTEXT->target_lid > HOST->opa_lid ) {
        CONTEXT->insert_at = i + 1;
        return true;
    }
    else if ( CONTEXT->target_lid == HOST->opa_lid ) {
        CONTEXT->search_okay = false;
    }
    return false;
}

bool
opa_host_list_add_host(
    ptr_list_ref    hosts,
    opa_host_ref    host
)
{
    struct opa_host_lid_search_context context = {
                    .target_lid = host->opa_lid,
                    .insert_at = 0,
                    .search_okay = true
                };
                
    // Keep the hosts sorted by LID:
    ptr_list_iterate(hosts, __opa_host_lid_search, &context);
    if ( context.search_okay ) {
        ptr_list_insert(hosts, host, context.insert_at);
        return true;
    }
    return false;
}

//

int
__opa_host_by_lid_comparator(
    unsigned int    i,
    const void      *host,
    const void      *ptr_to_lid
)
{
    opa_host_t      *HOST = (opa_host_ref)host;
    STL_LID         *LID = (STL_LID*)ptr_to_lid;
    
    if ( HOST->opa_lid == *LID ) return 0;
    if ( HOST->opa_lid > *LID ) return 1;
    return -1;
}

opa_host_ref
opa_host_by_lid(
    ptr_list_ref    hosts,
    STL_LID         a_lid
)
{
    return (opa_host_ref)ptr_list_bsearch(hosts, __opa_host_by_lid_comparator, &a_lid);
}

//

bool
__opa_host_list_iterator_print(
    unsigned int    i,
    const void      *host,
    const void      *dummy
)
{
    opa_host_t      *HOST = (opa_host_ref)host;
    
    fprintf(
            stderr,
            "DEBUG: OPA host [%05d:0x%016llX] %s\n",
            HOST->opa_lid,
            HOST->opa_guid,
            HOST->name
        );
}

void
opa_host_list_print(
    ptr_list_ref    hosts
)
{
    ptr_list_iterate(hosts, __opa_host_list_iterator_print, NULL);
}
