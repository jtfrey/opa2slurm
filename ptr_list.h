/*
 * ptr_list.h
 *
 * Array of pointer values.
 *
 */

#ifndef __PTR_LIST_H__
#define __PTR_LIST_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

typedef const void* (*ptr_list_ptr_copy)(const void *ptr);
typedef void (*ptr_list_ptr_dealloc)(const void *ptr);

typedef struct ptr_list * ptr_list_ref;

ptr_list_ref ptr_list_alloc(unsigned int capacity, ptr_list_ptr_copy copy_ptr, ptr_list_ptr_dealloc dealloc_ptr);


void ptr_list_push(ptr_list_ref a_ptr_list, const void *new_ptr);
void ptr_list_insert(ptr_list_ref a_ptr_list, const void *new_ptr, unsigned int at_index);


typedef bool (*ptr_list_iterator)(unsigned int index, const void *a_ptr, const void *context);
bool ptr_list_iterate(ptr_list_ref a_ptr_list, ptr_list_iterator iterator, const void *context);


typedef enum {
    kptr_list_search_state_continue = 0,
    kptr_list_search_state_found,
    kptr_list_search_state_break
} ptr_list_search_state_t;
typedef ptr_list_search_state_t (*ptr_list_search_iterator)(unsigned int index, const void *a_ptr, const void *context);
const void* ptr_list_search(ptr_list_ref a_ptr_list, ptr_list_search_iterator search_iterator, const void *context);


typedef int (*ptr_list_bsearch_comparator)(unsigned int index, const void *a_ptr, const void *context);
const void* ptr_list_bsearch(ptr_list_ref a_ptr_list, ptr_list_bsearch_comparator bsearch_comparator, const void *context);


void ptr_list_clear(ptr_list_ref a_ptr_list);
void ptr_list_dealloc(ptr_list_ref a_ptr_list);


unsigned int ptr_list_count(ptr_list_ref a_ptr_list);
unsigned int ptr_list_capacity(ptr_list_ref a_ptr_list);
const void* ptr_list_ptr_at_index(ptr_list_ref a_ptr_list, unsigned int index);

void ptr_list_remove_ptr_at_index(ptr_list_ref a_ptr_list, unsigned int index);
void ptr_list_remove_ptr(ptr_list_ref a_ptr_list, const void *ptr);

#endif /* __PTR_LIST_H__ */
