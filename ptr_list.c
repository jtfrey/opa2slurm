/*
 * ptr_list.c
 *
 * Array of pointer values.
 *
 */

#include "ptr_list.h"

typedef struct ptr_list {
    unsigned int            count, capacity;
    const void*             *ptrs;
    ptr_list_ptr_copy       copy;
    ptr_list_ptr_dealloc    dealloc;
} ptr_list_t;

unsigned int __ptr_list_adjust_capacity(
    unsigned int    capacity
)
{
    return (16 * ((capacity / 16) + ((capacity % 16) != 0)));
}

ptr_list_ref
ptr_list_alloc(
    unsigned int            capacity,
    ptr_list_ptr_copy       copy_ptr,
    ptr_list_ptr_dealloc    dealloc_ptr
)
{
    ptr_list_t      *new_list = malloc(sizeof(ptr_list_t));

    if ( new_list ) {
        new_list->count = 0;
        new_list->dealloc = dealloc_ptr;
        new_list->copy = copy_ptr;
        if ( capacity > 0 ) {
            capacity = __ptr_list_adjust_capacity(capacity);
            new_list->ptrs = malloc(sizeof(const void*) * capacity);
            if ( ! new_list->ptrs ) {
                free((void*)new_list);
                new_list = NULL;
            } else {
                new_list->capacity = capacity;
            }
        } else {
            new_list->capacity = 0;
            new_list->ptrs = NULL;
        }
    }
    return new_list;
}

//

void
ptr_list_push(
    ptr_list_ref    a_ptr_list,
    const void      *new_ptr
)
{
    if ( a_ptr_list->count == a_ptr_list->capacity ) {
        unsigned int    new_capacity = __ptr_list_adjust_capacity(a_ptr_list->capacity + 1);
        const void*     *new_ptrs = realloc(a_ptr_list->ptrs, sizeof(const void*) * new_capacity);

        if ( new_ptrs ) {
            a_ptr_list->ptrs = new_ptrs;
            a_ptr_list->capacity = new_capacity;
        } else {
            fprintf(stderr, "ERROR:  unable to allocate memory while growing pointer list\n");
            exit(ENOMEM);
        }
    }
    a_ptr_list->ptrs[a_ptr_list->count++] = ( a_ptr_list->copy ? a_ptr_list->copy(new_ptr) : new_ptr );
}

//

void
ptr_list_insert(
    ptr_list_ref    a_ptr_list,
    const void      *new_ptr,
    unsigned int    at_index
)
{
    if ( a_ptr_list->count == a_ptr_list->capacity ) {
        unsigned int    new_capacity = __ptr_list_adjust_capacity(a_ptr_list->capacity + 1);
        const void*     *new_ptrs = realloc(a_ptr_list->ptrs, sizeof(const void*) * new_capacity);

        if ( new_ptrs ) {
            a_ptr_list->ptrs = new_ptrs;
            a_ptr_list->capacity = new_capacity;
        } else {
            fprintf(stderr, "ERROR:  unable to allocate memory while growing pointer list\n");
            exit(ENOMEM);
        }
    }

    if ( at_index < 0 ) at_index = 0;
    else if ( at_index > a_ptr_list->count ) at_index = a_ptr_list->count;

    if ( at_index == 0 ) {
        if ( a_ptr_list->count ) memmove(&a_ptr_list->ptrs[1], &a_ptr_list->ptrs[0], a_ptr_list->count * sizeof(const void*));
        a_ptr_list->ptrs[0] = ( a_ptr_list->copy ? a_ptr_list->copy(new_ptr) : new_ptr );
        a_ptr_list->count++;
    }
    else if ( at_index == a_ptr_list->count ) {
        a_ptr_list->ptrs[a_ptr_list->count++] = ( a_ptr_list->copy ? a_ptr_list->copy(new_ptr) : new_ptr );
    }
    else {
        memmove(&a_ptr_list->ptrs[at_index + 1], &a_ptr_list->ptrs[at_index], (a_ptr_list->count - at_index) * sizeof(const void*));
        a_ptr_list->ptrs[at_index] = ( a_ptr_list->copy ? a_ptr_list->copy(new_ptr) : new_ptr );
        a_ptr_list->count++;

    }
}

//

bool
ptr_list_iterate(
    ptr_list_ref        a_ptr_list,
    ptr_list_iterator   iterator,
    const void          *context
)
{
    bool                rc = true;
    unsigned int        i = 0;

    while ( rc && (i < a_ptr_list->count) ) {
        rc = iterator(i, a_ptr_list->ptrs[i], context);
        i++;
    }
    return rc;
}

//

const void*
ptr_list_search(
    ptr_list_ref                a_ptr_list,
    ptr_list_search_iterator    search_iterator,
    const void                  *context
)
{
    unsigned int                i = 0;

    while ( i < a_ptr_list->count ) {
        switch ( search_iterator(i, a_ptr_list->ptrs[i], context) ) {

            case kptr_list_search_state_continue:
                i++;
                break;

            case kptr_list_search_state_found:
                return a_ptr_list->ptrs[i];

            case kptr_list_search_state_break:
                i = a_ptr_list->count;
                break;

        }
    }
    return NULL;
}

//

const void*
ptr_list_bsearch(
    ptr_list_ref                a_ptr_list,
    ptr_list_bsearch_comparator bsearch_comparator,
    const void                  *context
)
{
    if ( a_ptr_list->count > 0 ) {
        long int                l = 0, r = a_ptr_list->count - 1, m;

        while ( l <= r ) {
            int                 d;

            m = (l + r) / 2;
            d = bsearch_comparator(m, a_ptr_list->ptrs[m], context);
            if ( d == 0 ) return a_ptr_list->ptrs[m];

            // Exit now if the left and right indices have met:
            if ( l == r ) break;

            if ( d < 0 ) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
    }
    return NULL;
}

//

void
ptr_list_clear(
    ptr_list_ref    a_ptr_list
)
{
    if ( a_ptr_list->dealloc ) {
        unsigned int    i = 0;
        while ( a_ptr_list->count ) {
            a_ptr_list->dealloc(a_ptr_list->ptrs[--a_ptr_list->count]);
        }
    } else {
        a_ptr_list->count = 0;
    }
}

//

void
ptr_list_dealloc(
    ptr_list_ref    a_ptr_list
)
{
    if ( a_ptr_list->ptrs ) {
        ptr_list_clear(a_ptr_list);
        free((void*)a_ptr_list->ptrs);
    }
    free((void*)a_ptr_list);
}

//

unsigned int
ptr_list_count(
    ptr_list_ref    a_ptr_list
)
{
    return a_ptr_list->count;
}

//

unsigned int
ptr_list_capacity(
    ptr_list_ref    a_ptr_list
)
{
    return a_ptr_list->capacity;
}

//

const void*
ptr_list_ptr_at_index(
    ptr_list_ref    a_ptr_list,
    unsigned int    index
)
{
    if ( index < a_ptr_list->count ) return a_ptr_list->ptrs[index];
    return NULL;
}

//

void
ptr_list_remove_ptr_at_index(
    ptr_list_ref    a_ptr_list,
    unsigned int    index
)
{
    if ( index < a_ptr_list->count ) {
        if ( a_ptr_list->dealloc ) a_ptr_list->dealloc(a_ptr_list->ptrs[index]);
        // Handle each special case:
        if ( index == 0 ) {
            if ( --a_ptr_list->count > 0 ) memmove(&a_ptr_list->ptrs[0], &a_ptr_list->ptrs[1], a_ptr_list->count * sizeof(const void*));
        }
        else if ( index == a_ptr_list->count - 1 ) {
            a_ptr_list->count--;
        }
        else {
            memmove(&a_ptr_list->ptrs[index], &a_ptr_list->ptrs[index + 1], (a_ptr_list->count - index - 1) * sizeof(const void*));
            a_ptr_list->count--;
        }
    }
}

//

void
ptr_list_remove_ptr(
    ptr_list_ref    a_ptr_list,
    const void      *ptr
)
{
    if ( a_ptr_list->count > 0 ) {
        unsigned int    i = a_ptr_list->count;
        
        while ( i > 0 ) {
            if ( a_ptr_list->ptrs[--i] == ptr ) ptr_list_remove_ptr_at_index(a_ptr_list, i);
        }
    }
}
