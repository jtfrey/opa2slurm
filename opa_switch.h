/*
 * opa_switch.h
 *
 * OPA switch record.
 */

#ifndef __OPA_SWITCH_H__
#define __OPA_SWITCH_H__

#include "opa_host.h"

typedef struct opa_switch * opa_switch_ref;

opa_switch_ref opa_switch_alloc(const char *name, STL_LID opa_lid, uint64_t opa_guid);
opa_switch_ref opa_switch_retain(opa_switch_ref sw);
void opa_switch_release(opa_switch_ref sw);

const char* opa_switch_name(opa_switch_ref sw);
STL_LID opa_switch_lid(opa_switch_ref sw);
uint64_t opa_switch_guid(opa_switch_ref sw);

unsigned int opa_switch_connected_switch_count(opa_switch_ref sw);
ptr_list_ref opa_switch_connected_switches(opa_switch_ref sw);
unsigned int opa_switch_connected_host_count(opa_switch_ref sw);
ptr_list_ref opa_switch_connected_hosts(opa_switch_ref sw);

bool opa_switch_add_connected_host(opa_switch_ref sw, opa_host_ref host);
bool opa_switch_add_connected_switch(opa_switch_ref sw, opa_switch_ref connected_sw);
bool opa_switch_remove_connected_switch(opa_switch_ref sw, opa_switch_ref connected_sw);

bool opa_switch_list_add_switch(ptr_list_ref switches, opa_switch_ref sw);
opa_switch_ref opa_switch_by_lid(ptr_list_ref switches, STL_LID a_lid);
void opa_switch_list_print(ptr_list_ref switches);

#endif /* __OPA_SWITCH_H__ */
