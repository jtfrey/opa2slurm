/*
 * opa_switch.c
 *
 * OPA switch record.
 */

#include "opa_switch.h"

typedef struct opa_switch {
    unsigned int        refcnt;
    STL_LID             opa_lid;
    uint64_t            opa_guid;
    char                *name;
    ptr_list_ref        host_list;
    ptr_list_ref        switch_list;
} opa_switch_t;

//

opa_switch_ref
opa_switch_alloc(
    const char      *name,
    STL_LID         opa_lid,
    uint64_t        opa_guid
)
{
    size_t          name_len = 0;
    
    // Isolate the name to the first whitespace:
    while ( name[name_len] && ! isspace(name[name_len]) ) name_len++;
    
    opa_switch_t    *new_switch = malloc(sizeof(opa_switch_t) + (name_len + 1));
    
    if ( new_switch ) {
        void        *base = ((void*)new_switch) + sizeof(opa_switch_t);
        
        new_switch->refcnt = 1;
        new_switch->opa_lid = opa_lid;
        new_switch->opa_guid = opa_guid;
        new_switch->host_list = NULL;
        new_switch->switch_list = NULL;
        
        new_switch->name = base; base += name_len;
        strncpy(new_switch->name, name, name_len);
        new_switch->name[name_len] = '\0';
    }
    return new_switch;
}

//

opa_switch_t*
opa_switch_retain(
    opa_switch_ref  sw
)
{
    sw->refcnt++;
    return sw;
}

//

void
opa_switch_release(
    opa_switch_ref  sw
)
{
    if ( --sw->refcnt == 0 ) {
        if ( sw->switch_list ) ptr_list_dealloc(sw->switch_list);
        if ( sw->host_list ) ptr_list_dealloc(sw->host_list);
    }
}

//

const char*
opa_switch_name(
    opa_switch_ref  sw
)
{
    return sw->name;
}

//

STL_LID
opa_switch_lid(
    opa_switch_ref  sw
)
{
    return sw->opa_lid;
}

//

uint64_t
opa_switch_guid(
    opa_switch_ref  sw
)
{
    return sw->opa_guid;
}

//

ptr_list_ref
opa_switch_connected_switches(
    opa_switch_ref  sw
)
{
    return sw->switch_list;
}

//

ptr_list_ref
opa_switch_connected_hosts(
    opa_switch_ref  sw
)
{
    return sw->host_list;
}

//

struct opa_switch_lid_search_context {
    STL_LID         target_lid;
    unsigned int    insert_at;
    bool            search_okay;
};

bool
__opa_switch_lid_search(
    unsigned int    i,
    const void      *sw,
    const void      *context
)
{
    opa_switch_t    *SW = (opa_switch_t*)sw;
    struct opa_switch_lid_search_context *CONTEXT = (struct opa_switch_lid_search_context*)context;
    
    if ( CONTEXT->target_lid > SW->opa_lid ) {
        CONTEXT->insert_at = i + 1;
        return true;
    }
    else if ( CONTEXT->target_lid == SW->opa_lid ) {
        CONTEXT->search_okay = false;
    }
    return false;
}

bool
opa_switch_list_add_switch(
    ptr_list_ref    switches,
    opa_switch_ref  sw
)
{
    struct opa_switch_lid_search_context context = {
                    .target_lid = sw->opa_lid,
                    .insert_at = 0,
                    .search_okay = true
                };
                
    // Keep the switches sorted by LID:
    ptr_list_iterate(switches, __opa_switch_lid_search, &context);
    if ( context.search_okay ) {
        ptr_list_insert(switches, sw, context.insert_at);
        return true;
    }
    return false;
}

//

int
__opa_switch_by_lid_comparator(
    unsigned int    i,
    const void      *sw,
    const void      *ptr_to_lid
)
{
    opa_switch_t    *SW = (opa_switch_t*)sw;
    STL_LID         *LID = (STL_LID*)ptr_to_lid;
    
    if ( SW->opa_lid == *LID ) return 0;
    if ( SW->opa_lid > *LID ) return 1;
    return -1;
}

opa_switch_t*
opa_switch_by_lid(
    ptr_list_ref    switches,
    STL_LID         a_lid
)
{
    return (opa_switch_t*)ptr_list_bsearch(switches, __opa_switch_by_lid_comparator, &a_lid);
}

//

bool
__opa_switch_list_iterator_print(
    unsigned int    i,
    const void      *sw,
    const void      *dummy
)
{
    opa_switch_t    *SW = (opa_switch_t*)sw;
    
    fprintf(
            stderr,
            "DEBUG: OPA SWITCH [%05d:0x%016llX] %s\n",
            SW->opa_lid,
            SW->opa_guid,
            SW->name
        );
}

void
opa_switch_list_print(
    ptr_list_ref    switches
)
{
    ptr_list_iterate(switches, __opa_switch_list_iterator_print, NULL);
}

//

unsigned int
opa_switch_connected_host_count(
    opa_switch_ref  sw
)
{
    if ( sw->host_list ) return ptr_list_count(sw->host_list);
    return 0;
}

//

bool
opa_switch_add_connected_host(
    opa_switch_ref  sw,
    opa_host_ref    host
)
{
    if ( ! sw->host_list ) {
        sw->host_list = ptr_list_alloc(
                                24,
                                (ptr_list_ptr_copy)opa_host_retain,
                                (ptr_list_ptr_dealloc)opa_host_release
                            );
    } else {
        // Already there?
        if ( opa_host_by_lid(sw->host_list, opa_host_lid(host)) ) return true;
    }
    return opa_host_list_add_host(sw->host_list, host);
}

//

unsigned int
opa_switch_connected_switch_count(
    opa_switch_ref  sw
)
{
    if ( sw->switch_list ) return ptr_list_count(sw->switch_list);
    return 0;
}

//

bool
opa_switch_add_connected_switch(
    opa_switch_ref  sw,
    opa_switch_ref  connected_sw
)
{
    // Can't connect a switch to itself:
    if ( sw->opa_lid == connected_sw->opa_lid ) return false;
    
    if ( ! sw->switch_list ) {
        sw->switch_list = ptr_list_alloc(
                                8,
                                (ptr_list_ptr_copy)opa_switch_retain,
                                (ptr_list_ptr_dealloc)opa_switch_release
                            );
    } else {
        // Already present?
        if ( opa_switch_by_lid(sw->switch_list, connected_sw->opa_lid) ) return true;
    }
    return opa_switch_list_add_switch(sw->switch_list, connected_sw);
}

//

bool
opa_switch_remove_connected_switch(
    opa_switch_ref  sw,
    opa_switch_ref  connected_sw
)
{
    if ( sw->switch_list ) ptr_list_remove_ptr(sw->switch_list, connected_sw);
}
