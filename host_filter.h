/*
 * host_filter.h
 *
 * Filter OPA node names by Slurm hostlist, regex, etc.
 */

#ifndef __HOST_FILTER_H__
#define __HOST_FILTER_H__

#include "opa2slurm-config.h"
#include <stdbool.h>

typedef struct host_filter * host_filter_ref;

host_filter_ref host_filter_alloc(void);
host_filter_ref host_filter_retain(host_filter_ref the_filter);
void host_filter_release(host_filter_ref the_filter);

#ifdef USE_SLURM_HOSTLISTS
bool host_filter_add_hostlist(host_filter_ref the_filter, const char *hostlist_str);
#endif

bool host_filter_add_regex(host_filter_ref the_filter, const char *regex_str);

bool host_filter_add_fnmatch(host_filter_ref the_filter, const char *glob_str);

bool host_filter_matches_name(host_filter_ref the_filter, const char *name_str);

#endif /* __HOST_FILTER_H__ */
