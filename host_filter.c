/*
 * host_filter.c
 *
 * Filter OPA node names by Slurm hostlist, regex, etc.
 */

#include "host_filter.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>

#define _GNU_SOURCE
#include <fnmatch.h>

#ifdef USE_SLURM_HOSTLISTS
#include <slurm/slurm.h>
#endif

//

typedef void (*host_filter_dealloc_callback)(void *filter_impl);
typedef bool (*host_filter_matches_callback)(void *filter_impl, const char *s);

//

typedef struct host_filter_impl {
    struct host_filter_impl       *next_impl;
    host_filter_dealloc_callback  dealloc_fn;
    host_filter_matches_callback  matches_fn;
} host_filter_impl_t;

//

void
host_filter_impl_init(
    host_filter_impl_t            *the_impl,
    host_filter_dealloc_callback  dealloc_fn,
    host_filter_matches_callback  matches_fn
)
{
    the_impl->next_impl = NULL;
    the_impl->dealloc_fn = dealloc_fn;
    the_impl->matches_fn = matches_fn;
}

//

void
host_filter_impl_dealloc(
    host_filter_impl_t            *the_impl
)
{
    if ( the_impl->dealloc_fn ) {
        the_impl->dealloc_fn(the_impl);
    }
    free((void*)the_impl);
}

//
#if 0
#pragma mark -
#endif

#ifdef USE_SLURM_HOSTLISTS

typedef struct hostlist_host_filter_impl {
    host_filter_impl_t            base;
    hostlist_t                    hostlist;
} hostlist_host_filter_impl_t;

//

void
hostlist_host_filter_impl_dealloc(
    void                          *filter_impl
)
{
    hostlist_host_filter_impl_t   *FILTER = (hostlist_host_filter_impl_t*)filter_impl;
    
    if ( FILTER->hostlist ) slurm_hostlist_destroy(FILTER->hostlist);
}

//

bool
hostlist_host_filter_impl_matches(
    void                          *filter_impl,
    const char                    *s
)
{
    hostlist_host_filter_impl_t   *FILTER = (hostlist_host_filter_impl_t*)filter_impl;
    
    if ( FILTER->hostlist && (slurm_hostlist_find(FILTER->hostlist, s) != -1) ) return true;
    return false;
}

//

host_filter_impl_t*
hostlist_host_filter_impl_alloc(
    const char      *hostlist_str
)
{
    host_filter_impl_t  *new_impl = NULL;
    hostlist_t          h = slurm_hostlist_create(hostlist_str);
    
    if ( h ) {
        hostlist_host_filter_impl_t   *new_filter = malloc(sizeof(hostlist_host_filter_impl_t));
        
        if ( new_filter ) {
            new_impl = (host_filter_impl_t*)new_filter;
            
            host_filter_impl_init(&new_filter->base, hostlist_host_filter_impl_dealloc, hostlist_host_filter_impl_matches);
            new_filter->hostlist = h;
        } else {
            slurm_hostlist_destroy(h);
        }
    }
    return new_impl;
}

#endif

//
#if 0
#pragma mark -
#endif

typedef struct regex_host_filter_impl {
    host_filter_impl_t            base;
    regex_t                       regex;
} regex_host_filter_impl_t;

//

void
regex_host_filter_impl_dealloc(
    void                          *filter_impl
)
{
    regex_host_filter_impl_t      *FILTER = (regex_host_filter_impl_t*)filter_impl;
    
    regfree(&FILTER->regex);
}

//

bool
regex_host_filter_impl_matches(
    void                        *filter_impl,
    const char                  *s
)
{
    regex_host_filter_impl_t    *FILTER = (regex_host_filter_impl_t*)filter_impl;
    
    if ( regexec(&FILTER->regex, s, 0, NULL, 0) == 0 ) return true;
    return false;
}

//

host_filter_impl_t*
regex_host_filter_impl_alloc(
    const char      *regex_str
)
{
    host_filter_impl_t          *new_impl = NULL;
    regex_host_filter_impl_t    *new_filter = malloc(sizeof(regex_host_filter_impl_t));
        
    if ( new_filter ) {
        host_filter_impl_init(&new_filter->base, regex_host_filter_impl_dealloc, regex_host_filter_impl_matches);
        if ( regcomp(&new_filter->regex, regex_str, REG_EXTENDED | REG_ICASE | REG_NOSUB) != 0 ) {
            free((void*)new_filter);
        } else {
            new_impl = (host_filter_impl_t*)new_filter;
        }
    }
    return new_impl;
}

//
#if 0
#pragma mark -
#endif

typedef struct fnmatch_host_filter_impl {
    host_filter_impl_t            base;
    const char                    *pattern;
} fnmatch_host_filter_impl_t;

//

bool
fnmatch_host_filter_impl_matches(
    void                        *filter_impl,
    const char                  *s
)
{
    fnmatch_host_filter_impl_t  *FILTER = (fnmatch_host_filter_impl_t*)filter_impl;
    
    if ( fnmatch(FILTER->pattern, s, FNM_CASEFOLD) == 0 ) return true;
    return false;
}

//

host_filter_impl_t*
fnmatch_host_filter_impl_alloc(
    const char      *glob_str
)
{
    host_filter_impl_t          *new_impl = NULL;
    size_t                      glob_str_len = strlen(glob_str);
    fnmatch_host_filter_impl_t     *new_filter = malloc(sizeof(fnmatch_host_filter_impl_t) + glob_str_len + 1);
        
    if ( new_filter ) {
        host_filter_impl_init(&new_filter->base, NULL, fnmatch_host_filter_impl_matches);
        new_filter->pattern = ((void*)new_filter) + sizeof(fnmatch_host_filter_impl_t);
        strcpy((char*)new_filter->pattern, glob_str);
        new_impl = (host_filter_impl_t*)new_filter;
    }
    return new_impl;
}

//
#if 0
#pragma mark -
#endif

typedef struct host_filter {
    unsigned int        refcnt;
    host_filter_impl_t  *filter_list;
} host_filter_t;

//

host_filter_ref host_filter_alloc(void)
{
    host_filter_t     *new_filter = malloc(sizeof(host_filter_t));
    
    if ( new_filter ) {
        new_filter->refcnt = 1;
        new_filter->filter_list = NULL;
    }
    return new_filter;
}

//

host_filter_ref
host_filter_retain(
    host_filter_ref   the_filter
)
{
    the_filter->refcnt = 1;
    return the_filter;
}

//

void
host_filter_release(
    host_filter_ref   the_filter
)
{
    if ( --(the_filter->refcnt) == 0 ) {
        host_filter_impl_t    *f = the_filter->filter_list;
        
        while ( f ) {
            host_filter_impl_t  *next_impl = f->next_impl;
            
            host_filter_impl_dealloc(f);
            f = next_impl;
        }
        free((void*)the_filter);
    }
}

//

#ifdef USE_SLURM_HOSTLISTS
bool
host_filter_add_hostlist(
    host_filter_ref   the_filter,
    const char        *hostlist_str
)
{
    host_filter_impl_t*   new_filter = hostlist_host_filter_impl_alloc(hostlist_str);
    
    if ( new_filter ) {
        new_filter->next_impl = the_filter->filter_list;
        the_filter->filter_list = new_filter;
        return true;
    }
    return false;
}
#endif

//

bool
host_filter_add_regex(
    host_filter_ref   the_filter,
    const char        *regex_str
)
{
    host_filter_impl_t*   new_filter = regex_host_filter_impl_alloc(regex_str);
    
    if ( new_filter ) {
        new_filter->next_impl = the_filter->filter_list;
        the_filter->filter_list = new_filter;
        return true;
    }
    return false;
}

//

bool
host_filter_add_fnmatch(
    host_filter_ref   the_filter,
    const char        *glob_str
)
{
    host_filter_impl_t*   new_filter = fnmatch_host_filter_impl_alloc(glob_str);
    
    if ( new_filter ) {
        new_filter->next_impl = the_filter->filter_list;
        the_filter->filter_list = new_filter;
        return true;
    }
    return false;
}

//

bool
host_filter_matches_name(
    host_filter_ref   the_filter,
    const char        *name_str
)
{
    host_filter_impl_t    *f = the_filter->filter_list;
        
    while ( f ) {
        if ( f->matches_fn(f, name_str) ) return true;
        f = f->next_impl;
    }
    return false;
}

