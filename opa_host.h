/*
 * opa_host.h
 *
 * OPA host record.
 */

#ifndef __OPA_HOST_H__
#define __OPA_HOST_H__

#include <opamgt.h>
#include <opamgt_sa.h>

#include "ptr_list.h"

typedef struct opa_host * opa_host_ref;

opa_host_ref opa_host_alloc(const char *name, STL_LID opa_lid, uint64_t opa_guid);
opa_host_ref opa_host_retain(opa_host_ref host);
void opa_host_release(opa_host_ref host);

const char* opa_host_name(opa_host_ref host);
STL_LID opa_host_lid(opa_host_ref host);
uint64_t opa_host_guid(opa_host_ref host);

bool opa_host_list_add_host(ptr_list_ref hosts, opa_host_ref host);
opa_host_ref opa_host_by_lid(ptr_list_ref hosts, STL_LID a_lid);
void opa_host_list_print(ptr_list_ref hosts);

#endif /* __OPA_HOST_H__ */
