//
// opa2slurm
//
// Enumerate the nodes present on an OPA network and
// generate a SLURM topology configuration based on the
// relationships between those nodes.
//

#include "opa2slurm-config.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>

#include <opamgt.h>
#include <opamgt_sa.h>

#ifdef USE_SLURM_HOSTLISTS
#include <slurm/slurm.h>
#endif

#include "ptr_list.h"
#include "opa_host.h"
#include "opa_switch.h"
#include "host_filter.h"

//

const char* hfi_id_kind_str[] = {
                "HFI name and port",
                "HFI index and port",
                "HFI port GUID"
            };

typedef enum {
    khfi_id_kind_name   = 0, 
    khfi_id_kind_num    = 1,
    khfi_id_kind_guid   = 2
} hfi_id_kind_t;

typedef union {
    const char      *hfi_name;
    int32_t         hfi_num;
    uint64_t        hfi_guid;
} hfi_id_t;

//

bool                is_verbose = false;
bool                should_debug = false;

hfi_id_kind_t       hfi_kind = khfi_id_kind_num;
hfi_id_t            hfi_id = { .hfi_num = 0 };
int32_t             hfi_port = 0;

bool                should_eliminate_non_leaf_redundancy = true;
bool                should_emit_comments = true;
bool                should_do_linkspeed = false;
FILE                *output_file = NULL;

#ifdef USE_SLURM_HOSTLISTS
bool                should_do_ranged_lists = true;
#endif

host_filter_ref     excludes = NULL;

//

enum {
  opa2slurm_shortopt_exclude_hostlist     = 0x100,
  opa2slurm_shortopt_exclude_regex        = 0x101,
  opa2slurm_shortopt_exclude_fnmatch      = 0x102
};

static struct option opa2slurm_options[] = {
    {"help",                    no_argument,        NULL,   'h' },
    
    {"hfi-name",                required_argument,  NULL,   'N' },
    {"hfi-num",                 required_argument,  NULL,   'n' },
    {"hfi-port",                required_argument,  NULL,   'P' },
    
    {"port-guid",               required_argument,  NULL,   'G' },
    
    {"output",                  required_argument,  NULL,   'o' },
    {"verbose",                 no_argument,        NULL,   'v' },
    {"debug",                   no_argument,        NULL,   'd' },
    {"linkspeed",               no_argument,        NULL,   'L' },
    {"no-comments",             no_argument,        NULL,   'C' },
    {"no-redundancy-removal",   no_argument,        NULL,   'r' },
#ifdef USE_SLURM_HOSTLISTS
    {"no-ranged-lists",         no_argument,        NULL,   'R' },
    {"exclude-hostlist",        required_argument,  NULL,   opa2slurm_shortopt_exclude_hostlist },
#endif
    {"exclude-regex",           required_argument,  NULL,   opa2slurm_shortopt_exclude_regex },
    {"exclude-glob",            required_argument,  NULL,   opa2slurm_shortopt_exclude_fnmatch },
    {NULL,                      0,                  NULL,   0  }
};

//

#ifdef USE_SLURM_HOSTLISTS
const char *opa2slurm_optstring = "hN:n:P:G:o:vdCLrR";
#else
const char *opa2slurm_optstring = "hN:n:P:G:o:vdCLr";
#endif

//

void
usage(
    const char      *argv0
)
{
    printf(
        "usage:\n"
        "\n"
        "  %s {options}\n"
        "\n"
        "  [HFI selection]\n"
        "\n"
        "    -N, --hfi-name <hfi_name>    use the named HFI (e.g. hfi1_0)\n"
        "    -n, --hfi-num <#>            use the HFI by integer index (0 = first active)\n"
        "    -P, --hfi-port <#>           use the given port number on the HFI\n"
        "    -G, --port-guid <guid>       use the port with the given GUID\n"
        "                                 (e.g. 0x00117500d9000140)\n"
        "\n"
        "  -o, --output <path>            write output topology configuration\n"
        "                                 to the file at the given path\n"
        "  -C, --no-comments              do not emit comments in the generated\n"
        "                                 topology configuration\n"
#ifdef USE_SLURM_HOSTLISTS
        "  -R, --no-ranged-lists          do not produce ranged name lists a'la SLURM\n"
        "  --exclude-hostlist <hostlist>  exclude names in the SLURM hostlist from the topology\n"
#endif
        "  --exclude-regex <regex>        exclude host names matching the regex from the topology\n"
        "  --exclude-glob <pattern>       exclude host names matching the fnmatch pattern from the\n"
        "                                 topology\n"
        "  -L, --linkspeed                include LinkSpeed values for switches\n"
        "  -r, --no-redundancy-removal    do not remove references to non-leaf switches from\n"
        "                                 leaf switches\n"
        "  -v, --verbose                  display additional information to stderr\n"
        "\n"
        "  [version %d.%d]\n"
        "\n",
        argv0,
        opa2slurm_VERSION_MAJOR, opa2slurm_VERSION_MINOR
      );
}

//

void output_header(
    FILE        *output_file
)
{
    if ( should_emit_comments ) {
        fprintf(output_file,
            "#\n"
            "# topology.conf\n"
            "# Slurm switch configuration\n"
            "#\n"
            "# Generated by opa2slurm <http://github.com/jtfrey/opa2slurm>\n"
            "#\n"
            "\n"
          );
    }
}

//

OMGT_STATUS_T
opa_link_speed(
    struct omgt_port    *the_mgmt_port,
    STL_LID             switch_lid,
    uint64_t            *speed
)
{
    // A switch's relative performance is gauged by finding its link rate and width
    // and returning the product:
    OMGT_STATUS_T       rc;
    omgt_sa_selector_t  info_selector = {
                            .InputType = InputTypeLid,
                            .InputValue.NodeRecord.Lid = switch_lid
                        };
    int                 num_records = 0;
    STL_PORTINFO_RECORD *portinfo_records = NULL;
    
    rc = omgt_sa_get_portinfo_records(the_mgmt_port, &info_selector, &num_records, &portinfo_records);
    if ( rc == OMGT_STATUS_SUCCESS ) {
        if ( num_records > 0 ) {
            uint64_t    max_speed = 0, min_speed = (uint64_t)-1;
            int         i;
            
            for ( i = 0; i < num_records; i++ ) {
                uint64_t    port_speed = portinfo_records[i].PortInfo.LinkSpeed.Active * portinfo_records[i].PortInfo.LinkWidth.Active;
                if ( port_speed > max_speed ) max_speed = port_speed;
                if ( port_speed < min_speed ) min_speed = port_speed;
            }
            *speed = max_speed;
        }
        omgt_sa_free_records(portinfo_records);
    }
    return rc;
}

//

int
main(
    int                     argc,
    char**                  argv
)
{
    struct omgt_port        *the_mgmt_port = NULL;
    struct omgt_params      the_mgmt_params = {
                                .error_file = NULL,
                                .debug_file = NULL
                            };
    OMGT_STATUS_T           rc;
    const char              *opa_op = NULL;
    int                     optc;
    
    excludes = host_filter_alloc();

    while( (optc = getopt_long(argc, argv, opa2slurm_optstring, opa2slurm_options, NULL)) != -1 ) {
        switch(optc) {
        
            case 'h': {
                usage(argv[0]);
                exit(0);
            }
            
            case 'N': {
                if ( optarg && *optarg ) {
                    hfi_kind = khfi_id_kind_name;
                    hfi_id.hfi_name = optarg;
                } else {
                    fprintf(stderr, "ERROR:  an HFI name must be provided with the --hfi_name/-N option\n");
                    exit(EINVAL);
                }
                break;
            }
            
            case 'n': {
                if ( optarg && *optarg ) {
                    char*     endptr;
                    long      parsed_int = strtol(optarg, &endptr, 10);
                    
                    if ( endptr && (endptr > optarg) ) {
                        hfi_kind = khfi_id_kind_num;
                        hfi_id.hfi_num = parsed_int;
                    } else {
                        fprintf(stderr, "ERROR:  invalid HFI index: %s\n", optarg);
                        exit(EINVAL);
                    }
                } else {
                    fprintf(stderr, "ERROR:  a valid HFI index must be provided with the --hfi-num/-n option\n");
                    exit(EINVAL);
                }
                break;
            }
            
            case 'P': {
                if ( optarg && *optarg ) {
                    char*     endptr;
                    long      parsed_int = strtol(optarg, &endptr, 10);
                    
                    if ( endptr && (endptr > optarg) ) {
                        hfi_port = parsed_int;
                    } else {
                        fprintf(stderr, "ERROR:  invalid HFI port number: %s\n", optarg);
                        exit(EINVAL);
                    }
                } else {
                    fprintf(stderr, "ERROR:  a valid HFI port number must be provided with the --hfi-port/-P option\n");
                    exit(EINVAL);
                }
                break;
            }
            
            case 'G': {
                if ( optarg && *optarg ) {
                    char*               endptr;
                    unsigned long long  parsed_val = strtoull(optarg, &endptr, 0);
                    
                    if ( endptr && (endptr > optarg) ) {
                        hfi_kind = khfi_id_kind_guid;
                        hfi_id.hfi_guid = parsed_val;
                    } else {
                        fprintf(stderr, "ERROR:  invalid HFI GUID: %s\n", optarg);
                        exit(EINVAL);
                    }
                } else {
                    fprintf(stderr, "ERROR:  a valid HFI GUID must be provided with the --port-guid/-G option\n");
                    exit(EINVAL);
                }
                break;
            }
            
            case 'o': {
                if ( optarg && *optarg ) {
                    if ( strcmp(optarg, "-") == 0 ) {
                        output_file = stdout;
                    } else if ( (output_file = fopen(optarg, "w")) == NULL ) {
                        fprintf(stderr, "ERROR:  unable to open output file for write (errno = %d)\n", errno);
                        exit(errno);
                    }
                } else {
                    fprintf(stderr, "ERROR:  a filename must be provided with the --output/-o option\n");
                    exit(EINVAL);
                }
                break;
            }
            
            case 'v':
                is_verbose = true;
                break;
            
            case 'd':
                should_debug = true;
                break;
            
            case 'C':
                should_emit_comments = false;
                break;
              
            case 'L':
                should_do_linkspeed = true;
                break;
              
            case 'r':
                should_eliminate_non_leaf_redundancy = false;
                break;
            
#ifdef USE_SLURM_HOSTLISTS
            case 'R':
                should_do_ranged_lists = false;
                break;
            
            case opa2slurm_shortopt_exclude_hostlist:
                if ( ! host_filter_add_hostlist(excludes, optarg) ) {
                    fprintf(stderr, "ERROR:  unable to add --exclude-hostlist:  %s\n", optarg);
                    exit(EINVAL);
                }
                break;
#endif
            
            case opa2slurm_shortopt_exclude_regex:
                if ( ! host_filter_add_regex(excludes, optarg) ) {
                    fprintf(stderr, "ERROR:  unable to add --exclude-regex:  %s\n", optarg);
                    exit(EINVAL);
                }
                break;
            
            case opa2slurm_shortopt_exclude_fnmatch:
                if ( ! host_filter_add_fnmatch(excludes, optarg) ) {
                    fprintf(stderr, "ERROR:  unable to add --exclude-glob:  %s\n", optarg);
                    exit(EINVAL);
                }
                break;
                
            default:
                usage(argv[0]);
                exit(EINVAL);
            
        }
    }
    
    if ( output_file == NULL ) output_file = stdout;
    
    if ( is_verbose ) the_mgmt_params.error_file = stderr;
    if ( should_debug ) the_mgmt_params.debug_file = stderr;
    
    if ( is_verbose ) fprintf(stderr, "INFO:  opening managment connection by %s: ", hfi_id_kind_str[hfi_kind]);
    switch ( hfi_kind ) {
    
        case khfi_id_kind_name:
            if ( is_verbose ) fprintf(stderr, "%s %d\n", hfi_id.hfi_name, hfi_port);
            rc = omgt_open_port(&the_mgmt_port, (char*)hfi_id.hfi_name, hfi_port, &the_mgmt_params);
            break;
        
        case khfi_id_kind_num:
            if ( is_verbose ) fprintf(stderr, "%d %d\n", hfi_id.hfi_num, hfi_port);
            rc = omgt_open_port_by_num(&the_mgmt_port, hfi_id.hfi_num, hfi_port, &the_mgmt_params);
            break;
        
        case khfi_id_kind_guid:
            if ( is_verbose ) fprintf(stderr, "0x%016X\n", hfi_id.hfi_guid);
            rc = omgt_open_port_by_guid(&the_mgmt_port, hfi_id.hfi_guid, &the_mgmt_params);
            break;
    
    }
    
    STL_NODE_RECORD             *switch_records = NULL;
    STL_NODE_RECORD             *host_records = NULL;
    STL_LINK_RECORD             *link_records = NULL;
    
    ptr_list_ref                switches = ptr_list_alloc(8, (ptr_list_ptr_copy)opa_switch_retain, (ptr_list_ptr_dealloc)opa_switch_release);
    ptr_list_ref                hosts = ptr_list_alloc(32, (ptr_list_ptr_copy)opa_host_retain, (ptr_list_ptr_dealloc)opa_host_release);
        
    if ( rc == OMGT_STATUS_SUCCESS ) {
        omgt_sa_selector_t      info_selector;
        unsigned int            switch_count, host_count, link_count;
        int                     record_count = 0;
        
        // Select switch records only:
        info_selector.InputType = InputTypeNodeType;
        info_selector.InputValue.NodeRecord.NodeType = IBA_NODE_SWITCH;
        opa_op = "get switch information";
        rc = omgt_sa_get_node_records(the_mgmt_port, &info_selector, &record_count, &switch_records);
        if ( rc != OMGT_STATUS_SUCCESS ) goto exit_on_error;
        switch_count = record_count;
        if ( is_verbose ) fprintf(stderr, "INFO:  found %u switch(es) on fabric\n", switch_count);
        
        // If there are no switches, just exit:
        if ( switch_count > 0 ) {
            unsigned int        i;
            
            // Initialize switch objects:
            for ( i = 0; i < switch_count; i++ ) {
                opa_switch_ref  sw = opa_switch_alloc(
                                            switch_records[i].NodeDesc.NodeString,
                                            switch_records[i].RID.LID,
                                            switch_records[i].NodeInfo.NodeGUID
                                        );
                if ( ! sw ) {
                    fprintf(stderr, "ERROR:  unable to allocate switch object\n");
                    goto exit_on_error;
                }
                opa_switch_list_add_switch(switches, sw);
                opa_switch_release(sw);
                if ( is_verbose ) fprintf(stderr, "INFO:    switch_records[%d] = %s (LID %u)\n", i, switch_records[i].NodeDesc.NodeString, switch_records[i].RID.LID);
            }
            if ( should_debug ) opa_switch_list_print(switches);
            omgt_sa_free_records(switch_records);
            switch_records = NULL;
        
            // Select HFI records only:
            info_selector.InputType = InputTypeNodeType;
            info_selector.InputValue.NodeRecord.NodeType = IBA_NODE_CHANNEL_ADAPTER;
            opa_op = "get HFI information";
            rc = omgt_sa_get_node_records(the_mgmt_port, &info_selector, &record_count, &host_records);
            if ( rc != OMGT_STATUS_SUCCESS ) goto exit_on_error;
            host_count = record_count;
            if ( is_verbose ) fprintf(stderr, "INFO:  found %u HFI(s) on fabric\n", host_count);
            
            // Initialize host objects:
            for ( i = 0; i < host_count; i++ ) {
                opa_host_ref        host = opa_host_alloc(
                                                host_records[i].NodeDesc.NodeString,
                                                host_records[i].RID.LID,
                                                host_records[i].NodeInfo.NodeGUID
                                            );
                if ( ! host ) {
                    fprintf(stderr, "ERROR:  unable to allocate host object\n");
                    goto exit_on_error;
                }
                opa_host_list_add_host(hosts, host);
                opa_host_release(host);
                if ( is_verbose ) fprintf(stderr, "INFO:    host_records[%d] = %s (LID %u)\n", i, host_records[i].NodeDesc.NodeString, host_records[i].RID.LID);
            }
            if ( should_debug ) opa_host_list_print(hosts);
            omgt_sa_free_records(host_records);
            host_records = NULL;
            
            info_selector.InputType = InputTypeNoInput;
            opa_op = "get link information";
            rc = omgt_sa_get_link_records(the_mgmt_port, &info_selector, &record_count, &link_records);
            if ( rc != OMGT_STATUS_SUCCESS ) goto exit_on_error;
            link_count = record_count;
            if ( is_verbose ) fprintf(stderr, "INFO:  found %u link(s) on fabric\n", link_count);
            
            // Walk through the links:
            for ( i = 0; i < link_count; i++ ) {
                opa_switch_ref  sw1 = opa_switch_by_lid(switches, link_records[i].RID.FromLID);
                opa_host_ref    host1 = ( sw1 ? NULL : opa_host_by_lid(hosts, link_records[i].RID.FromLID));
                opa_switch_ref  sw2 = opa_switch_by_lid(switches, link_records[i].ToLID);
                opa_host_ref    host2 = ( sw2 ? NULL : opa_host_by_lid(hosts, link_records[i].ToLID));
                
                if ( sw1 ) {
                    // Connecting something to a switch:
                    if ( sw2 ) {
                        opa_switch_add_connected_switch(sw1, sw2);
                    }
                    else if ( host2 ) {
                        if ( ! host_filter_matches_name(excludes, opa_host_name(host2)) ) {
                            opa_switch_add_connected_host(sw1, host2);
                        } else if ( is_verbose ) {
                            fprintf(stderr, "INFO:  ignored host in exclude list:  %s\n", opa_host_name(host2));
                        }
                    }
                    else {
                        fprintf(stderr, "ERROR:  no entity on record for LID %d\n", link_records[i].ToLID);
                        goto exit_on_error;
                    }
                } else if ( host1 ) {
                    // Connecting a host to a switch:
                    if ( sw2 ) {
                        if ( ! host_filter_matches_name(excludes, opa_host_name(host1)) ) {
                            opa_switch_add_connected_host(sw2, host1);
                        } else if ( is_verbose ) {
                            fprintf(stderr, "INFO:  ignored host in exclude list:  %s\n", opa_host_name(host1));
                        }
                    }
                    else if ( host2 ) {
                        fprintf(stderr, "ERROR:  host-to-host direct connectivity???\n");
                        goto exit_on_error;
                    }
                    else {
                        fprintf(stderr, "ERROR:  no entity on record for LID %d\n", link_records[i].ToLID);
                        goto exit_on_error;
                    }
                } else {
                    fprintf(stderr, "ERROR:  no entity on record for LID %d\n", link_records[i].RID.FromLID);
                    goto exit_on_error;
                }
            }
            omgt_sa_free_records(link_records);
            link_records = NULL;
            
            //
            // We've now brought the switch topology in to memory.
            //
            unsigned int        iMax = ptr_list_count(switches);
            
            if ( should_eliminate_non_leaf_redundancy ) {
                //
                // First, walk the list and find any switches that have ZERO hosts on
                // them; remove any such switches from the connected switch lists of
                // switches with NON-ZERO host counts.
                //
                for ( i = 0; i < iMax; i++ ) {
                    unsigned int    j, jMax;
                    opa_switch_ref  sw = (opa_switch_ref)ptr_list_ptr_at_index(switches, i);
                    
                    if ( sw && (opa_switch_connected_host_count(sw) == 0) ) {
                        if ( is_verbose ) fprintf(stderr, "INFO:  non-leaf switch found: %s (%d:0x%016llX)\n", opa_switch_name(sw), opa_switch_lid(sw), opa_switch_guid(sw));
                        // Walk the switches and augment their connected switch lists:
                        for ( j = 0; j < iMax; j++ ) {
                            if ( j != i ) {
                                opa_switch_ref  conn_sw = (opa_switch_ref)ptr_list_ptr_at_index(switches, j);
                                
                                if ( conn_sw && (opa_switch_connected_host_count(conn_sw) > 0) ) {
                                    // Remove sw from conn_sw:
                                    opa_switch_remove_connected_switch(conn_sw, sw);
                                }
                            }
                        }
                    }
                }
            }
            
            //
            // Now we'll walk the list again and print the topology:
            //
            output_header(output_file);
            for ( i = 0; i < iMax; i++ ) {
                opa_switch_ref  sw = (opa_switch_ref)ptr_list_ptr_at_index(switches, i);
                
                if ( sw ) {
                    unsigned int    j, jMax;
                    
                    if ( should_emit_comments ) {
                        fprintf(
                                output_file,
                                "#\n"
                                "# Switch GUID 0x%016llX\n"
                                "#\n",
                                opa_switch_guid(sw)
                            );
                    }
                    fprintf(
                            output_file,
                            "SwitchName=%s",
                            opa_switch_name(sw)
                        );
                
                    ptr_list_ref    connected_switches = opa_switch_connected_switches(sw);
                    if ( connected_switches && ((jMax = ptr_list_count(connected_switches)) > 0) ) {
                        const char      *comma = "";
                        
                        fprintf(output_file, " Switches=");
#ifdef USE_SLURM_HOSTLISTS
                        hostlist_t      slurm_switch_list = NULL;
                        
                        if ( should_do_ranged_lists ) {
                            slurm_switch_list = slurm_hostlist_create("");
                            if ( ! slurm_switch_list ) {
                                fprintf(stderr, "ERROR:  unable to initialize SLURM-style switch list\n");
                                exit(ENOMEM);
                            }
                        }
#endif
                        for ( j = 0; j < jMax; j++ ) {
                            opa_switch_ref  connected_switch = (opa_switch_ref)ptr_list_ptr_at_index(connected_switches, j);
                            
                            if ( connected_switch ) {
#ifdef USE_SLURM_HOSTLISTS
                                if ( slurm_switch_list ) {
                                    if ( slurm_hostlist_push(slurm_switch_list, opa_switch_name(connected_switch)) != 1 ) {
                                        fprintf(stderr, "ERROR:  unable to add %s to SLURM switch list\n", opa_switch_name(connected_switch));
                                        exit(ENOMEM);
                                    }
                                } else {
#endif
                                    fprintf(output_file, "%s%s", comma, opa_switch_name(connected_switch));
                                    comma = ",";
#ifdef USE_SLURM_HOSTLISTS
                                }
#endif
                            }
                        }
#ifdef USE_SLURM_HOSTLISTS
                        if ( slurm_switch_list ) {
                            char        *switch_list_str;
                            
                            slurm_hostlist_uniq(slurm_switch_list);
                            switch_list_str = slurm_hostlist_ranged_string_malloc(slurm_switch_list);
                            if ( switch_list_str ) {
                                fprintf(output_file, "%s", switch_list_str);
                                free((void*)switch_list_str);
                            } else {
                                fprintf(stderr, "ERROR:  unable to generate SLURM-style ranged switch list\n");
                                exit(ENOMEM);
                            }
                            slurm_hostlist_destroy(slurm_switch_list);
                        }
#endif
                    }
                
                    ptr_list_ref    connected_hosts = opa_switch_connected_hosts(sw);
                    if ( connected_hosts && ((jMax = ptr_list_count(connected_hosts)) > 0) ) {
                        const char      *comma = "";
                        
                        fprintf(output_file, " Nodes=");
#ifdef USE_SLURM_HOSTLISTS
                        hostlist_t      slurm_host_list = NULL;
                        
                        if ( should_do_ranged_lists ) {
                            slurm_host_list = slurm_hostlist_create("");
                            if ( ! slurm_host_list ) {
                                fprintf(stderr, "ERROR:  unable to initialize SLURM-style host list\n");
                                exit(ENOMEM);
                            }
                        }
#endif
                        for ( j = 0; j < jMax; j++ ) {
                            opa_host_ref    connected_host = (opa_host_ref)ptr_list_ptr_at_index(connected_hosts, j);
                            
                            if ( connected_host ) {
#ifdef USE_SLURM_HOSTLISTS
                                if ( slurm_host_list ) {
                                    if ( slurm_hostlist_push(slurm_host_list, opa_host_name(connected_host)) != 1 ) {
                                        fprintf(stderr, "ERROR:  unable to add %s to SLURM host list\n", opa_host_name(connected_host));
                                        exit(ENOMEM);
                                    }
                                } else {
#endif
                                    fprintf(output_file, "%s%s", comma, opa_host_name(connected_host));
                                    comma = ",";
#ifdef USE_SLURM_HOSTLISTS
                                }
#endif
                            }
                        }
#ifdef USE_SLURM_HOSTLISTS
                        if ( slurm_host_list ) {
                            char        *host_list_str;
                            
                            slurm_hostlist_uniq(slurm_host_list);
                            host_list_str = slurm_hostlist_ranged_string_malloc(slurm_host_list);
                            if ( host_list_str ) {
                                fprintf(output_file, "%s", host_list_str);
                                free((void*)host_list_str);
                            } else {
                                fprintf(stderr, "ERROR:  unable to generate SLURM-style ranged host list\n");
                                exit(ENOMEM);
                            }
                            slurm_hostlist_destroy(slurm_host_list);
                        }
#endif
                    }
                    
                    if ( should_do_linkspeed ) {
                        uint64_t        speed = 0;
                        
                        if ( opa_link_speed(the_mgmt_port, opa_switch_lid(sw), &speed) == OMGT_STATUS_SUCCESS ) {
                            fprintf(output_file, " LinkSpeed=%llu", speed);
                        }
                    }
                    
                    fprintf(output_file, ( should_emit_comments ? "\n\n" : "\n" ));
                }
            }
        }
    } else {
        fprintf(stderr, "ERROR:  unable to connect to HFI (status %d): %s\n", rc, omgt_status_totext(rc));
    }

early_exit:

    //
    // Cleanup any allocations:
    //
    if ( switch_records ) omgt_sa_free_records(switch_records);
    if ( host_records ) omgt_sa_free_records(host_records);
    if ( link_records ) omgt_sa_free_records(link_records);
    if ( switches ) ptr_list_dealloc(switches);
    if ( hosts ) ptr_list_dealloc(hosts);

    //
    // Close the OPA management port:
    //
    if ( the_mgmt_port ) omgt_close_port(the_mgmt_port);
    
    //
    // Close the output file:
    //
    if ( output_file != stdout ) fclose(output_file);

    return 0;

exit_on_error:

    if ( rc != OMGT_STATUS_SUCCESS ) fprintf(stderr, "ERROR:  unable to %s (status %d): %s\n", opa_op, rc, omgt_status_totext(rc));

    //
    // Cleanup any allocations:
    //
    if ( switch_records ) omgt_sa_free_records(switch_records);
    if ( host_records ) omgt_sa_free_records(host_records);
    if ( link_records ) omgt_sa_free_records(link_records);
    if ( switches ) ptr_list_dealloc(switches);
    if ( hosts ) ptr_list_dealloc(hosts);

    //
    // Close the OPA management port:
    //
    if ( the_mgmt_port ) omgt_close_port(the_mgmt_port);
    
    //
    // Close the output file:
    //
    if ( output_file != stdout ) fclose(output_file);

    return 1;
}
