# opa2slurm

Once upon a time there was `ib2slurm`, a program that utilized the IB net discovery library to find all switches and channel adapters (CAs) to automatically generate a topology configuration file for Slurm.

Intel's OPA variant of the Infiniband standard does not include the net discovery library.  Rather, they include their own library for enumerating aspects of the OPA network(s) attached to a node.

This project is a rewrite of `ib2slurm` to use Intel's enumeration library.

## Building opa2slurm

-  Prerequisite packages:
   -  opa-libopamgt, opa-libopamgt-devel
   -  these two packages are present in both the IFS and Basic IntelOPA kits
      -  in the `INSTALL` script, the "OPA Tools" and "OFA OPA Development" components should be selected
      -  one could also install the RPMs directly, without access the `INSTALL` script
   -  users report that version 10.6.1.0.2 works properly, but older versions (e.g. 10.5.0.0.140) do not; I wrote this code against the 10.7.0.0.133 version
-  Checkout the git repository:

```
git clone https://gitlab.com/jtfrey/opa2slurm.git
```

-  Inside the repository, create a build directory:

```
$ cd opa2slurm
$ mkdir build
$ cd build
```

-  Configure the build using `cmake` or `ccmake`
   -  By default, CMake will install the `opa2slurm` executable to `/usr/local/bin`; to change the installation prefix, pass `-DCMAKE_INSTALL_PREFIX=<directory>` to CMake
   -  To use Slurm host list expressions, the `slurm/slurm.h` header file and Slurm library (e.g. `libslurm.so`) must:
      -  be installed under the system directories (`/usr/include`, `/usr/lib64` or `/usr/lib`)
      -  be installed under a directory provided to CMake (e.g. `-DSLURM_PREFIX=/opt/slurm`)
   -  If Slurm is installed under the system directories and you *do not* want to use its host list expressions, pass `-DUSE_SLURM_HOSTLISTS=Off` to CMake
   -  The OPA SDK (`opamgt/opamgt.h` and `libopamgt.so`) must:
      -  be installed under the system directories (`/usr/include`, `/usr/lib64` or `/usr/lib`)
      -  be installed under a directory provided to CMake (e.g. `-DOPA_PREFIX=/opt/opa`)

```
$ cmake -DCMAKE_INSTALL_PREFIX=/tmp/opa2slurm ..
-- The C compiler identification is GNU 4.8.5
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Looking for omgt_close_port
-- Looking for omgt_close_port - found
-- Looking for omgt_open_port
-- Looking for omgt_open_port - found
-- Looking for omgt_open_port_by_guid
-- Looking for omgt_open_port_by_guid - found
-- Looking for omgt_open_port_by_num
-- Looking for omgt_open_port_by_num - found
-- Looking for omgt_sa_free_records
-- Looking for omgt_sa_free_records - found
-- Looking for omgt_sa_get_link_records
-- Looking for omgt_sa_get_link_records - found
-- Looking for omgt_sa_get_node_records
-- Looking for omgt_sa_get_node_records - found
-- Looking for omgt_sa_get_portinfo_records
-- Looking for omgt_sa_get_portinfo_records - found
-- Looking for omgt_status_totext
-- Looking for omgt_status_totext - found
-- Configuring done
-- Generating done
-- Build files have been written to: /home/1001/opa2slurm/build
```

- Build the program
  -  After a successful build, the program will be present as `opa2slurm` inside the `build` directory

```
 make
Scanning dependencies of target opa2slurm
[ 25%] Building C object CMakeFiles/opa2slurm.dir/ptr_list.c.o
[ 50%] Building C object CMakeFiles/opa2slurm.dir/opa_host.c.o
[ 75%] Building C object CMakeFiles/opa2slurm.dir/opa_switch.c.o
[100%] Building C object CMakeFiles/opa2slurm.dir/opa2slurm.c.o
Linking C executable opa2slurm
[100%] Built target opa2slurm
```

- (Optional) install the program

```
$ make install
[100%] Built target opa2slurm
Install the project...
-- Install configuration: ""
-- Installing: /tmp/opa2slurm/bin/opa2slurm
```

## Running opa2slurm

Built-in help is present in `opa2slurm`:

```
$ ./opa2slurm --help
usage:

  ./opa2slurm {options}

  [HFI selection]

    -N, --hfi-name <hfi_name>    use the named HFI (e.g. hfi1_0)
    -n, --hfi-num <#>            use the HFI by integer index (0 = first active)
    -P, --hfi-port <#>           use the given port number on the HFI
    -G, --port-guid <guid>       use the port with the given GUID
                                 (e.g. 0x00117500d9000140)

  -o, --output <path>            write output topology configuration
                                 to the file at the given path
  -C, --no-comments              do not emit comments in the generated
                                 topology configuration
  --exclude-regex <regex>        exclude host names matching the regex from the topology
  --exclude-glob <pattern>       exclude host names matching the fnmatch pattern from the
                                 topology
  -L, --linkspeed                include LinkSpeed values for switches
  -r, --no-redundancy-removal    do not remove references to non-leaf switches from
                                 leaf switches
  -v, --verbose                  display additional information to stderr

  [version 0.1]

```

Since it is possible for a host to have multiple OPA subnets present (e.g. one per OPA port present on the host), the **HFI selection** options allow the selection of a specific adapter (by `--hfi-name` or `--hfi-num`) and port on that adapter.  If you know the port's GUID, the `--port-guid` option can be used in lieu of the `--hfi-*` arguments.  The program defaults to using the equivalent of `--hfi-num=0 --hfi-port=0` which essentially selects the "first" OPA port on the host.  The default behavior is likely adequate for most systems.

By default, the topology is written to stdout.  The `--output` option is used to direct the output to an arbitrary file; note that using `-` as the path indicates stdout.

The generated topology will contain comments indicating each switch's GUID and when the file was generated.  For a more compact topology file, use the `--no-comments` option.

The built-in help does not mention one additional command line option:  `-d`/`--debug` enables extremely verbose output, including OPAMGT dumps of the binary data it collects and the steps it takes to do so.  The `--verbose` option increases the amount of information `opa2slurm` communicates to the user (via stderr) as it runs.

### Link Speed

Slurm documentation claims that the LinkSpeed value one can assign to each switch is currently unused by the topology plugin.  The OPA SDK represents link speed with two values:  link width and signaling rate.  These two parameters are bit vectors, with each bit representing a capability (thus, the active width and rate use a single bit while the set of permissible widths and rates may have multiple bits set).  The established trend is for new capabilities to extend the defined bits, so new capabilities equate to larger integer values.  So to an extent, using the product of link width and signaling rate will always (haha) give a relative measure of performance.

### Slurm host lists

If `opa2slurm` is compiled and linked against the Slurm library, the `Switches=` and `Nodes=` lists in the topology will by default have redundancies removed and be compacted in accordance with host list expression syntax.  E.g. the list `r00n00,r00n01,r00n02,r00n01` would be output as `r00n[00-02]`.  Additional CLI options will be present, as well:

```
$ ./opa2slurm --help
usage:

  ./opa2slurm {options}

  [HFI selection]

    -N, --hfi-name <hfi_name>    use the named HFI (e.g. hfi1_0)
    -n, --hfi-num <#>            use the HFI by integer index (0 = first active)
    -P, --hfi-port <#>           use the given port number on the HFI
    -G, --port-guid <guid>       use the port with the given GUID
                                 (e.g. 0x00117500d9000140)

  -o, --output <path>            write output topology configuration
                                 to the file at the given path
  -C, --no-comments              do not emit comments in the generated
                                 topology configuration
  -R, --no-ranged-lists          do not produce ranged name lists a'la SLURM
  --exclude-hostlist <hostlist>  exclude names in the SLURM hostlist from the topology
  --exclude-regex <regex>        exclude host names matching the regex from the topology
  --exclude-glob <pattern>       exclude host names matching the fnmatch pattern from the
                                 topology
  -L, --linkspeed                include LinkSpeed values for switches
  -r, --no-redundancy-removal    do not remove references to non-leaf switches from
                                 leaf switches
  -v, --verbose                  display additional information to stderr

  [version 0.1]

```
